import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title: string = 'TodoList';
  add: Array<string>;
  completed: Array<string>;
  incomplete: Array<string>;
  bandAdd: boolean;
  bandAll: boolean;
  bandCompleted: boolean;
  bandIncomplete: boolean;
  dato: string;

  constructor() {
    this.add = new Array();
    this.completed = new Array();
    this.incomplete = new Array();
    this.bandAll  = false;
    this.bandAdd = false;
    this.bandCompleted = false;
    this.bandIncomplete = false;
  }

  clickAdd() {
    this.bandAdd = true;
    this.bandAll  = false;
    this.bandCompleted = false;
    this.bandIncomplete = false;
    this.add.push(this.dato);
    this.incomplete.push(this.dato);
    this.dato = '';
  }

  clickAll() {
    this.bandAll = true;
    this.bandAdd = false;
    this.bandCompleted = false;
    this.bandIncomplete = false;
    this.add = new Array();
  }

  clickCompleted() {
    this.bandAll  = false;
    this.bandAdd = false;
    this.bandCompleted = true;
    this.bandIncomplete = false;
    this.add = new Array();
  }

  clickIncomplete() {
    this.bandAll  = false;
    this.bandAdd = false;
    this.bandCompleted = false;
    this.bandIncomplete = true;
    this.add = new Array();
  }

  todoIncomplete(i) {
    let indice: number = this.incomplete.indexOf(i);

    this.completed.push(this.incomplete[indice]);
    this.incomplete.splice(indice, 1);
  }

  todoCompleted(i) {
    this.completed.splice(this.completed.indexOf(i), 1);
  }
}
